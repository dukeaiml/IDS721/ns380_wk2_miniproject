# ns380_wk2_miniproject
Data Loader: Scrapes NBA game data from espn.com and returns it directly to the user

Future work:
- Extract boxscore in entirity
- Store data in CSVs in S3 and return a presigned URL to the user

Usage Instructions:
Run a command like
```
curl -H "Content-Type: application/json" --data '{"datetime": "2022-04-01"}' https://2zlrb6xo94.execute-api.us-west-2.amazonaws.com/default/ns380-lambda
```
This should display the NBA games that were played on that day, using a lambda to scrape espn.com to get the information. The lambda is configured through
API Gateway which takes in the relevant information.

A test JSON is included. To use this, simply change the command above to
```
curl -H "Content-Type: application/json" --data @test.json https://2zlrb6xo94.execute-api.us-west-2.amazonaws.com/default/ns380-lambda
```
after downloading test.json to your local current working directory.
