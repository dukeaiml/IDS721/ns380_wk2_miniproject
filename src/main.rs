use tracing_subscriber::filter::{EnvFilter, LevelFilter};
use lambda_http::{run, service_fn, Body, Error, Request, Response};
use serde::{Deserialize, Serialize};
use serde_json::from_str;
use chrono::{NaiveDate, Local};

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct RequestBody {
    pub datetime: String,
}

/// This is the main body for the function.
/// Write your code inside it.
/// There are some code example in the following URLs:
/// - https://github.com/awslabs/aws-lambda-rust-runtime/tree/main/examples
async fn function_handler(event: Request) -> Result<Response<Body>, Error> {
    // Extract some useful information from the request
    let body = event.body();
    let s = std::str::from_utf8(body).expect("invalid utf-8 sequence");

    //Serialze JSON into struct.
    //If JSON is incorrect, send back 400 with error.
    let item = match from_str::<RequestBody>(s) {
        Ok(item) => item,
        Err(err) => {
            let resp = Response::builder()
                .status(400)
                .header("content-type", "text/html")
                .body(("Please make sure your body is formatted as {'datetime': 'YYYY-MM-DD'} ".to_string() + &err.to_string()).into())
                .map_err(Box::new)?;
            return Ok(resp);
        }
    };

    // Validate that the date is actually a real date
    let date = match NaiveDate::parse_from_str(item.datetime.as_str(), "%Y-%m-%d") {
        Ok(date) => date,
        Err(err) => {
            let resp = Response::builder()
                .status(400)
                .header("content-type", "text/html")
                .body(("Please make sure your body is formatted as {'datetime': 'YYYY-MM-DD'} ".to_string() + &err.to_string()).into())
                .map_err(Box::new)?;
            return Ok(resp);
        }
    };

    // Validate that the date is today or in the past
    let today = Local::now().date_naive();
    if today < date {
        let resp = Response::builder()
            .status(400)
            .header("content-type", "text/html")
            .body("Please make sure your date is not in the future".to_string().into())
            .map_err(Box::new)?;
        return Ok(resp);
    }

    let mut vec: Vec<String> = Vec::new();
    {
        let response = reqwest::get(format!("https://www.espn.com/nba/scoreboard/_/date/{}", date.format("%Y%m%d").to_string())).await?;
        let html_content = response.text().await?;
        let document = scraper::Html::parse_document(&html_content);
        let game_link_selector = scraper::Selector::parse("section.Scoreboard").unwrap();
        for node in document.select(&game_link_selector) {
            for subnode in node.select(&scraper::Selector::parse("a").unwrap()){
                let x = subnode.value().attr("href").map(str::to_owned).unwrap();
                if x.contains("boxscore") {
                    vec.push(x);       
                    break;
                }
            }
        }   
    }
    if vec.len() == 0 {
        let resp = Response::builder()
            .status(400)
            .header("content-type", "text/html")
            .body("No games were played on this date".to_string().into())
            .map_err(Box::new)?;
        return Ok(resp);
    }
    
    let mut out: String = "".to_owned();
    // Not doing this rn because we have a deadline but should be making a new async function here
    // and then doing this async for each item and waiting for all futures at the end.
    for x in vec.iter() {
        let v_response = reqwest::get(format!("https://www.espn.com{}", x)).await?;
        let v_html_content = v_response.text().await?;
        let v_document = scraper::Html::parse_document(&v_html_content);
        let mut teams: Vec<String> = Vec::new();
        let boxscore_selector = scraper::Selector::parse("div.Boxscore").unwrap();
        for v_node in v_document.select(&boxscore_selector) {
            for v_subnode in v_node.select(&scraper::Selector::parse("div.BoxscoreItem__TeamName").unwrap()){
                let team = v_subnode.text().next().unwrap().to_owned();
                teams.push(team);
            }
        }
        // TODO: extract the other boxscore information and put it into a CSV. Put the CSVs in S3 and return presigned URL
        // to the CSVs in S3
        out.push_str(&teams[0]);
        out.push_str(" vs ");
        out.push_str(&teams[1]);
        out.push_str("\n");
    }

    // Return something that implements IntoResponse.
    // It will be serialized to the right response event automatically by the runtime
    let resp = Response::builder()
        .status(200)
        .header("content-type", "text/plain")
        .body(out.to_string().into())
        .map_err(Box::new)?;
    Ok(resp)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_env_filter(
            EnvFilter::builder()
                .with_default_directive(LevelFilter::INFO.into())
                .from_env_lossy(),
        )
        // disable printing the name of the module in every log line.
        .with_target(false)
        // disabling time is handy because CloudWatch will add the ingestion time.
        .without_time()
        .init();

    run(service_fn(function_handler)).await
}
